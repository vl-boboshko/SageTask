/**
 * Created by Vladislav Boboshko on 28.10.2015.
 */

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Task {

    private static final Sage[] sages = new Sage[5];
    private static final Lock[] sticks = new Lock[5];

    public static void main(String[] args) {
        init();
        for (Sage i : sages) {
            i.start();
        }
        destroy(0);
    }

    public static void init() {
        for (int i = 0; i < 5; i++) {
            sticks[i] = new ReentrantLock();
        }
        sages[0] = new Sage(sticks[4], sticks[0], 1);
        for (int i = 1; i < 5; i++) {
            sages[i] = new Sage(sticks[i - 1], sticks[i], i + 1);
        }
    }

    public static void destroy(int seconds) {
        if (seconds < 1) {
            return;
        }
        try {
            Thread.sleep(seconds * 1000);
        } catch (Exception e) {
        } finally {
            for (Sage i : sages) {
                i.stop();
            }
        }
    }
}
