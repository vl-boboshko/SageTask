import java.util.Random;
import java.util.concurrent.locks.Lock;

/**
 * Created by Endiny on 28.10.2015.
 */
public class Sage extends Thread implements Runnable {
    private Lock leftStick;
    private Lock rightStick;
    private boolean state;
    private Integer index;
    private Random random = new Random();
    public Sage(Lock leftStick, Lock rightStick, Integer index) {
        state = false;
        setName("Sage #"+index);
        this.leftStick = leftStick;
        this.rightStick = rightStick;
        this.index = index;
        System.out.println("Sage #"+index+" is thinking");
    }

    private void tryToEat() {
        if (leftStick.tryLock()) {
            if (!rightStick.tryLock()) {
                leftStick.unlock();
                return;
            }
        }
        else {
            return;
        }
        try {
            System.out.println("Sage #"+index+": Eating");
            Thread.sleep((random.nextInt(3) + 3) * 1000);
        }
        catch (Exception e) {}
        finally {
            leftStick.unlock();
            rightStick.unlock();
            System.out.println("Sage #"+index+": Thinking");
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                sleep(1000);
            }
            catch (Exception e) {}
            if ((random.nextInt(100)+1) % 5 == 0) {
                tryToEat();
            }
        }
    }
}